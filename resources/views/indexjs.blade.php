<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">
        <link href="{{ asset('css/app.css')}}" rel="stylesheet">
        <title>Paginação</title>

    </head>
    <body>
        <div class="container">
           <div class="card text-center">
            <div class="card-header">
                Table de clientes
            </div>
            <div class="card-body">
            <h5 class="card-title" id="cardTitle">
            Exibindo 
            </h5>
                <table class="table table-hover" id="tabelaClientes">
                    <thead>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Sbrenome</th>
                        <th scope="col">E-mail</th>
                    </thead>
                    <tbody>
                   
                        <tr>
                            <td>1</td>
                            <td>Kaic</td>
                            <td>Melo</td>
                            <td>kaic@gmail.com</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Kaic</td>
                            <td>Melo</td>
                            <td>kaic@gmail.com</td>
                        </tr>
            
                    </tbody>
                </table>
               
            </div>
            <div class="card-footer">
            <nav id="paginator">
            <ul class="pagination">
                <!-- <li class="page-item">
                <a class="page-link" href="#" aria-label="Anterior">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Anterior</span>
                </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                <a class="page-link" href="#" aria-label="Próximo">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Próximo</span>
                </a>
                </li> -->
            </ul>
            </nav>
            </div>
           </div>
        </div>
        <script src="{{ asset('js/app.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
            //MONTANDO AS PAGINAÇÕES DA TABELA
                function getItemProximo(data){
                    i = data.current_page+1;
                    if(data.last_page == data.current_page){
                            s = '<li class="page-item active">';
                        }else{
                            s= '<li class="page-item">';
                            s+= "<a class='page-link'"+"pagina='"+i+"' href='javascript:void(0);'>Proxima</a></li>";
                        }
                        return s;
                }
                function getItemAnterior(data){
                    i = data.current_page-1;
                    if(i=== data.current_page){
                            s = '<li class="page-item active">';
                        }else{
                            s= '<li class="page-item">';
                            s+= "<a class='page-link'"+"pagina='"+i+"' href='javascript:void(0);'>Anterior</a></li>";
                        }
                        return s;
                }
                function montarPaginator(data){
                    $("#paginator>ul>li").remove();
                    $("#paginator>ul").append(getItemAnterior(data));

                    n = 10;

                    if(data.current_page - n/5 <=1)
                        inicio = 1;
                    else if(data.last_page - data.current_page <n)
                        inicio = data.last_page - n +1;
                    else
                        inicio = data.current_page - n/2;
                                
                    
                    fim = inicio +n-1;

                    for(i=inicio ; i<=fim ;i++){
                        if(i=== data.current_page){
                            s = '<li class="page-item active">';
                            s+= "<a class='page-link' href='javascript:void(0);'>"+i+"</a></li>";
                        }else{
                            s= '<li class="page-item">';
                            s+= "<a class='page-link'"+"pagina='"+i+"' href='javascript:void(0);'>"+i+"</a></li>";
                        }
                       $("#paginator>ul").append(s);
                    }

                    $("#paginator>ul").append(getItemProximo(data));
                }
            //MONTANDO AS LINNAS DA TABELA
                function montarLinha(cliente){
                    return '<tr>'+
                            '<td>'+cliente.id+'</td>'+
                            '<td>'+cliente.nome+'</td>'+
                            '<td>'+cliente.sobrenome+'</td>'+
                            '<td>'+cliente.email+'</td>'+
                        '</tr>';
                }
            //APAGANDO DADOS E MONTANDO NOVA TABELA
                function montarTabela(data){
                    $("#tabelaClientes>tbody>tr").remove();
                   
                    for (i=0; i<data.data.length; i++){
                        s = montarLinha(data.data[i]);
                        $("#tabelaClientes>tbody").append(s);
                    }
                }

            //CARREGAR OS ARQUIVO JSON  
                function carregarClientes(pagina){
                    $.get('{{url("/json")}}',{page:pagina},function(resp){
                        // console.log(resp);
                        montarTabela(resp);
                        montarPaginator(resp);

                        $("#paginator>ul>li>a").click(function(){
                            carregarClientes( $(this).attr('pagina'));
                        });

                        $("#cardTitle").html("Exibindo "+ resp.per_page + " clientes de " + resp.total+ 
                        " ( " + resp.from + " a " + resp.to+ ")");
                     });
                }
            //REALIZAR APÓS O CARREGAMENTO DA PAGINA 
                $(function(){
                    carregarClientes(1)
                });
        </script>
    </body> 
</html>
